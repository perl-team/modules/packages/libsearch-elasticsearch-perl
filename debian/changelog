libsearch-elasticsearch-perl (8.12-1) unstable; urgency=medium

  * Import upstream version 8.12.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 04 Feb 2024 04:03:40 +0100

libsearch-elasticsearch-perl (8.00-1) unstable; urgency=medium

  * Import upstream version 8.00.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Feb 2023 02:38:55 +0100

libsearch-elasticsearch-perl (7.717-1) unstable; urgency=medium

  * Import upstream version 7.717.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Refresh test and runtime dependencies.
    Add dual-lifed modules, and more JSON variants for tests.

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Aug 2022 19:48:21 +0200

libsearch-elasticsearch-perl (7.715-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
    * Build-Depends-Indep: Drop versioned constraint on libhttp-tiny-perl.
    * libsearch-elasticsearch-perl: Drop versioned constraint on
      libhttp-tiny-perl in Depends.

  [ gregor herrmann ]
  * Import upstream version 7.715.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Oct 2021 16:17:56 +0200

libsearch-elasticsearch-perl (7.714-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on liblog-any-perl,
      libmoo-perl.
    + libsearch-elasticsearch-perl: Drop versioned constraint on
      liblog-any-perl, libmoo-perl in Depends.

  [ Laurent Baillet ]
  * New upstream version 7.711001

  [ gregor herrmann ]
  * Update years of upstream copyright.

  [ Florian Schlichting ]
  * Import upstream version 7.714.
  * Declare compliance with Debian Policy 4.6.0

 -- Florian Schlichting <fsfs@debian.org>  Sun, 05 Sep 2021 21:48:38 +0800

libsearch-elasticsearch-perl (7.30-1) unstable; urgency=medium

  * Team upload.

  [ Laurent Baillet ]
  * New upstream version 7.30

  [ gregor herrmann ]
  * Update dependencies.
  * Add a note to debian/NEWS about this release supporting the
    Elasticsearch 7.0 branch.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.

 -- Laurent Baillet <laurent.baillet@gmail.com>  Tue, 10 Nov 2020 10:27:10 +0100

libsearch-elasticsearch-perl (6.81-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Laurent Baillet ]
  * Make Lintian rules-requires-root-missing pass
  * Declare compliance with Debian Policy 4.5.0
  * Use debhelper-compat and bump to version 12
  * New upstream version 6.81
  * Import upstream version 6.81
  * Enrico Zimuel replaces Clinton Gormley as developer
  * Add myself in debian/copyright
  * Update years of upstream copyright
  * Drop some minimum version dependencies

  [ gregor herrmann ]
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

 -- Laurent Baillet <laurent.baillet@gmail.com>  Mon, 29 Jun 2020 14:17:33 +0200

libsearch-elasticsearch-perl (6.00-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Drop debian/tests/pkg-perl/smoke-tests, handled by pkg-perl-
    autopkgtest now.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Mirko Tietgen ]
  * Import upstream version 6.00
  * remove obsolete spelling.patch
  * Set debhelper version to 11

  [ gregor herrmann ]
  * Update years of upstream and packaging copyright.
  * Bump versioned build dependency on libmoo-perl to 2.001000.
  * Declare compliance with Debian Policy 4.2.1.
  * Add debian/NEWS mentioning the API change from 5_0 to 6_0.
  * Use HTTPS fpr apache.org URL in debian/copyright.

 -- Mirko Tietgen <mirko@abunchofthings.net>  Thu, 17 May 2018 18:16:06 +0200

libsearch-elasticsearch-perl (5.01-1) unstable; urgency=low

  * Initial release (closes: #834363).

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Dec 2016 03:11:34 +0100
